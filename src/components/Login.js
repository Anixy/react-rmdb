import React, { useState, useContext } from "react";
import { useNavigate } from "react-router";

import API from '../API';
// Components
import Button from './Button';

//Styles
import { Wrapper } from "./Login.style";
//Context
import { Context } from "../context";



const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(false);
    const [errorRes, setErrorRes] = useState('');
    const [_user, setUser] = useContext(Context);
    const navigate = useNavigate();
    console.log(errorRes);
    const handleSubmit = async () => {
        setError(false);
        try {
            const requestToken = await API.getRequestToken();
            const sessionId = await API.authenticate(
                requestToken,
                username,
                password
            );
            if (sessionId.success === true ) {
                setUser({ sessionId: sessionId.session_id, username});
    
                navigate('/');
            } else {
                setErrorRes(sessionId.status_message);
                setError(true);
            }

            
        } catch {
            setError(true);
            console.log('masuk');
        }
    };

    const handleInput = e => {
        const name = e.currentTarget.name;
        const value = e.currentTarget.value;

        if (name === 'username') setUsername(value);
        if (name === 'password') setPassword(value);
    };

    return (
        <Wrapper>
            {error && !errorRes &&<div className="error">There was an error</div>}
            {error && errorRes && <div className="error">{errorRes}</div>}
            <label>Usernname:</label>
            <input
                type='text'
                value={username}
                name='username'
                onChange={handleInput}
            />
            <input
                type='password'
                value={password}
                name='password'
                onChange={handleInput}
            />
        <Button text="Login" callback={handleSubmit}/>
        </Wrapper>
    )
};

export default Login;



