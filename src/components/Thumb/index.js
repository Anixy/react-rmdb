import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

//Styles
import {Image, Rate} from './Thumb.style';

const Thumb = ({ rate, title, image, movieId, clickable }) => (
    <div>
        {clickable ? (
            <Link to={`/${movieId}`} >
                <Image src={image} alt='movie-thumb' />
            </Link>
        ) : (
            <Image src={image} alt='movie-thumb' />
        )}
        <Rate>{rate}</Rate>
        <h2>{title}</h2>
    </div>
)

Thumb.propTypes = {
    image: PropTypes.string,
    movieId: PropTypes.number,
    clickable: PropTypes.bool
}

export default Thumb;