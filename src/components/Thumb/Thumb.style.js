import styled from 'styled-components';

export const Image = styled.img`
    width: 100%;
    max-width: 720px;
    transition: all 0.3s;
    object-fit: cover;
    border-radius: 20px;
    animation: animateThumb 0.5s;
    z-index: 1;

    :hover {
        opacity: 0.8;
    }


    @keyframes animateThumb {
        from { 
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

`;

export const Rate = styled.div`
    position: relative;
    top: -40px;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 35px;
    height: 35px;
    background: var(--medGrey);
    color: var(--white);
    font-weight: 800;
    border-radius: 50%;
`;
